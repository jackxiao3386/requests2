//! # requests2 
//! `requests`可以使用requests风格来写http请求 
/// --snip--

pub mod requests; // requests data
pub mod parser; // parser data
pub mod cache; // store data as a mini database
pub mod value;
pub mod untils; // get data from store database


pub use self::requests::Requests;
pub use self::cache::Cache;
pub use self::cache::Store;
pub use self::value::Value;


