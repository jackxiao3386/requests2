/// ### Headers 描述http请求头 有三种请求默认default
/// 自定义格式json 会使用serde来处理此处的json 
/// None无请求头
#[derive(Debug, Copy, Clone)]
pub enum Headers {
    /// 通用的user-agent: '----'..
    Default,
    /// 自定义json 格式的 如r#"{'host': '', 'user-agent': ''}"#
    JSON(&'static str),
    /// 不带任何请求头
    None,
}
