#[derive(Debug, Clone, Copy)]
pub enum Proxies {
    /// 自定义代理
    STR(&'static str),
    /// 不带任何请求头
    None,
}