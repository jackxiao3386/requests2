
use std::collections::HashMap;

use proc_macro2::TokenStream;
use quote::{quote, quote_spanned, ToTokens};
use syn::spanned::Spanned;
use syn::{parse_macro_input, parse_quote, Data, DeriveInput, Fields, Generics, GenericParam, AttributeArgs, Meta};
use syn;



#[proc_macro_attribute]
pub fn dbnote(args: proc_macro::TokenStream, input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let attrs_args = parse_macro_input!(args as AttributeArgs);
    
    let mut map = HashMap::new();

    for i in attrs_args.iter() {
        match i {
            syn::NestedMeta::Meta(Meta::NameValue(ref j)) => {
                map.insert(
                    str::replace(&j.path.to_token_stream().to_string(), "\"", ""),
                    str::replace(&j.lit.to_token_stream().to_string(), "\"", ""),
                );
            }
            _ => unreachable!(""),
        }
    }


    let primary_key = map
        .get("primary_key")
        .unwrap_or(&String::from(""))
        .clone();

    let driver = map.get("driver").unwrap_or(&String::from("")).clone();

    let table_name = map
        .get("table_name")
        .unwrap_or(&String::from(""))
        .clone();

    
    let expanded = quote! {

        #input

        fn get_primary_key() -> String {
            #primary_key.to_string()
        }

        fn get_table_name() -> String {
            #table_name.to_string()
        }

        fn get_conn_str() -> String {
            // get connection string
            let driver = get_driver();
            let mut s = String::new();
            File::open("./config").unwrap().read_to_string(&mut s).unwrap();

            if driver == "postgres" {
                let reg = Regex::new("postgres[\\s]*=[\\s]*<([^>]+)>").unwrap();
                let caps = reg.captures(&s).expect("you use postgre as database, but config not setting!");
                (&caps[1]).to_string()

            }
            else if driver == "sqlite" {
                let reg = Regex::new("sqlite[\\s]*=[\\s]*<dbname=(.*)>").unwrap();
                let caps = reg.captures(&s).expect("you use sqlite as database, but config not setting!");
                (&caps[1]).to_string()
            }
            else {
                String::from("csv")
            }
        }

        fn get_driver() -> String {
            #driver.to_string()
        }
    };

    proc_macro::TokenStream::from(expanded)

}



#[proc_macro_derive(DBfile)]
pub fn dbfile_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;

    let generics = add_trait_bounds(input.generics);
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();


    let code = proc_write_line(&input.data);
    let code2 = proc_write_header(&input.data);

    // for database process
    let insert_db_fields_code = proc_db_fill_insert_fields(&input.data);
    let insert_db_value_code = proc_db_fill_insert_values(&input.data);
    let insert_db_my_fields_code = proc_db_fill_insert_myfields(&input.data);
    
    // create db
    let create_table_code = proc_db_create_table(&input.data);


    let expanded = quote! {
        use std::fs::File;
        use std::fs::OpenOptions;
        use std::io::Write;
        use std::io::ErrorKind;
        use postgres::{Client, NoTls, types::ToSql};
        use regex::Regex;
        use rusqlite::{params, Connection, Result};
        use std::io::Read;

        

        impl #impl_generics dbfile::DBfile for #name #ty_generics #where_clause {
            fn _build_create_table_code(&self) -> String {
                // create table 
                let create_table_sql = format!("CREATE TABLE IF NOT EXISTS {} {:?}", get_table_name().as_str(), (#create_table_code));
                let trim_the_code = str::replace(create_table_sql.as_str(), "\"", "");
    
                if get_primary_key().as_str() != "" {
                    // process primary key defined, your primary key don't appear at struct field tail
                    let reg_primary_key = format!("({}[^,]*),", get_primary_key().as_str());
    
                    let regex = Regex::new(&reg_primary_key).unwrap();
                    let data = regex.captures(&trim_the_code).unwrap();
                    let cap = &data[1];
                    let new_str = format!("{} {}", cap, "primary key not null");
    
                    str::replace(&trim_the_code, cap, &new_str)
                }
                else {
                    trim_the_code
                }
            }
    

            fn _test(&self) {
                let primary_key =  get_primary_key();
                let table_name = get_table_name();
                let driver = get_driver();
                let connstr = get_conn_str();

                println!("PK {:?}", primary_key);
                println!("TBNAME {:?}", table_name);
                println!("DRIVER {:?}", driver);
                println!("CONNSTR {:?}", connstr);
            }

            fn create_table(&self) {
                // use driver "csv" not create a table
                if get_driver().as_str() == "csv" {
                    panic!("you don't create table!");
                }
                if get_driver().as_str() == "postgres" {
                    // create conn
                    let mut conn = Client::connect(get_conn_str().as_str(), NoTls).unwrap();
                    let code = self._build_create_table_code();
                    conn.batch_execute(code.as_str()).unwrap();
                }
                else if get_driver().as_str() == "sqlite" {
                    let conn = Connection::open(get_conn_str().as_str()).unwrap();
                    let code = self._build_create_table_code();
                    conn.execute(code.as_str(), params![]).unwrap();
                }
            }

            fn to_db(&self) {
                // insert db code , here test postgresql
                let insert_code_str = str::replace(stringify!(#insert_db_fields_code), "usize", "");

                // insert code and execute insert
                let insert_sql = format!("insert into {} ({}) values ({})", get_table_name().as_str(), stringify!(#insert_db_my_fields_code), insert_code_str);
                
                if get_driver().as_str() == "postgres" {
                    let mut conn = Client::connect(get_conn_str().as_str(), NoTls).unwrap();
                    let result = conn.execute(insert_sql.as_str(), &[
                        #insert_db_value_code
                    ]);
                    match result {
                        Ok(s) => println!("insert success!"),
                        Err(err) => println!("erorr: {:?}", err)
                    } 
                }
                else if get_driver().as_str() == "sqlite" {
                    let mut conn = Connection::open(get_conn_str().as_str()).unwrap();
                    let result = conn.execute(insert_sql.as_str(), params![
                        #insert_db_value_code
                    ]);    
                    match result {
                        Ok(s) => println!("insert success!"),
                        Err(err) => println!("erorr: {:?}", err)
                    }
                }
            }

            fn write_csv_head(&self) {
                // println!("execute code {:?}", stringify!(#code));

                let mut file = OpenOptions::new().append(true).open(get_table_name().as_str()).unwrap_or_else(|error| {
                    if error.kind() == ErrorKind::NotFound {
                        File::create(get_table_name().as_str()).unwrap_or_else(|error| {
                            panic!("Problem creating the file: {:?}", error);
                            })
                        } else {
                            panic!("Problem opening the file: {:?}", error);
                        }
                    }
                ); 
                #code2
            }

            fn to_csv(&self, mode: &str)  {
                // println!("execute code {:?}", stringify!(#code));

                let mut is_append = false;
                if mode == "a" { is_append = true };
                
                let mut file = OpenOptions::new().append(is_append).open(get_table_name().as_str()).unwrap_or_else(|error| {
                    if error.kind() == ErrorKind::NotFound {
                        File::create(get_table_name().as_str()).unwrap_or_else(|error| {
                            panic!("Problem creating the file: {:?}", error);
                            })
                        } else {
                            panic!("Problem opening the file: {:?}", error);
                        }
                    }
                ); 
                #code
            }
        }
    };

    proc_macro::TokenStream::from(expanded)
}

fn add_trait_bounds(mut generics: Generics) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param.bounds.push(parse_quote!(hello_macro::HelloMacro));
        }
    }
    generics
}

fn proc_db_create_table(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => {
            match data.fields {
                Fields::Named(ref fields) => {
                    let count = fields.named.iter().count();

                    let recurse = fields.named.iter().enumerate().map(|(i, f)| {
                        let name = &f.ident;

                        if i == count - 1 {
                            quote_spanned! { f.span() => 
                                dbfile::DBfield::create_db_fields(&self.#name, stringify!(#name).to_string())
                            }
                        }
                        else {
                            quote_spanned! { f.span() => 
                                dbfile::DBfield::create_db_fields(&self.#name, stringify!(#name).to_string()),
                            }
                        }   
                    });

                    quote! {
                        #(#recurse)*
                    }
                }
                Fields::Unnamed(ref _fields) => panic!("not process unnamed fileds"),
                Fields::Unit => panic!("not process unit fileds")
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

fn proc_write_header(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => {
            match data.fields {
                Fields::Named(ref fields) => {
                    let count = fields.named.iter().count();

                    let recurse = fields.named.iter().enumerate().map(|(i, f)| {
                        let name = &f.ident;

                        quote_spanned! { f.span() =>
                            if #i == #count - 1 {
                                file.write(format!("{:#} \n", stringify!(#name)).as_bytes()).unwrap()
                            } else {
                                file.write(format!("{:#},", stringify!(#name)).as_bytes()).unwrap()
                            }
                            // self.#name.push_str("_蠢")
                        }
                    });

                    quote! {
                        #(#recurse;)*
                    }
                }
                Fields::Unnamed(ref _fields) => panic!("not process unnamed fileds"),
                Fields::Unit => panic!("not process unit fileds")
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

fn proc_db_fill_insert_myfields(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => {
            match data.fields {
                Fields::Named(ref fields) => {
                    let count = fields.named.iter().count();

                    let recurse = fields.named.iter().enumerate().map(|(i, f)| {
                        let name = &f.ident;

                        if i == count - 1 {
                            quote_spanned! { f.span() => #name}
                        }
                        else {
                            quote_spanned! { f.span() => #name,}
                        }
                    });

                    quote! {
                        #(#recurse)*
                    }
                }
                Fields::Unnamed(ref _fields) => panic!("not process unnamed fileds"),
                Fields::Unit => panic!("not process unit fileds")
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

fn proc_db_fill_insert_values(data:&Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => {
            match data.fields {
                Fields::Named(ref fields) => {
                    let count = fields.named.iter().count();

                    let recurse = fields.named.iter().enumerate().map(|(i, f)| {
                        let name = &f.ident;

                        if i == count - 1 {
                            quote_spanned! { f.span() => &self.#name}
                        }
                        else {
                            quote_spanned! { f.span() => &self.#name,}
                        }
                    });

                    quote! {
                        #(#recurse)*
                    }
                }
                Fields::Unnamed(ref _fields) => panic!("not process unnamed fileds"),
                Fields::Unit => panic!("not process unit fileds")
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

fn proc_db_fill_insert_fields(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => {
            match data.fields {
                Fields::Named(ref fields) => {
                    let count = fields.named.iter().count();

                    let recurse = fields.named.iter().enumerate().map(|(i, f)| {
                        let number = i + 1;

                        if i == count - 1 {
                            quote_spanned! { f.span() => $#number}
                        }
                        else {
                            quote_spanned! { f.span() => $#number,}
                        }
                    });

                    quote! {
                        #(#recurse)*
                    }
                }
                Fields::Unnamed(ref _fields) => panic!("not process unnamed fileds"),
                Fields::Unit => panic!("not process unit fileds")
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

fn proc_write_line(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => {
            match data.fields {
                Fields::Named(ref fields) => {
                    let count = fields.named.iter().count();

                    let recurse = fields.named.iter().enumerate().map(|(i, f)| {
                        let name = &f.ident;

                        quote_spanned! { f.span() =>
                            if #i == #count - 1 {
                                file.write(format!("{:#} \n", self.#name).as_bytes()).unwrap()
                            } else {
                                file.write(format!("{:#},", self.#name).as_bytes()).unwrap()
                            }
                        }
                    });

                    quote! {
                        #(#recurse;)*
                    }
                }
                Fields::Unnamed(ref _fields) => panic!("not process unnamed fileds"),
                Fields::Unit => panic!("not process unit fileds")
            }
        }
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}