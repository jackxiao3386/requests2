pub mod dbfield;

pub use self::dbfield::DBfield;


pub trait DBfile {
    fn _build_create_table_code(&self) -> String; // 辅助函数用于创建各种数据库表的通用代码
    fn _test(&self); // 测试函数

    fn write_csv_head(&self);
    fn to_csv(&self, mode: &str);


    fn create_table(&self);
    fn to_db(&self);
}

