
pub trait DBfield {
    fn create_db_fields(&self, name: String) -> String {
        format!("{} {}", name, "varchar(255)")
    }
}

impl DBfield for f32 {
    fn create_db_fields(&self, name: String) -> String {
        format!("{} {}", name, "REAL")
    }
}

impl DBfield for f64 {
    fn create_db_fields(&self, name: String) -> String {
        format!("{} {}", name, "REAL")
    }
}

impl DBfield for String {
    fn create_db_fields(&self, name: String) -> String {
        format!("{} {}", name, "TEXT")
    }
}

impl DBfield for &str {
    fn create_db_fields(&self, name: String) -> String {
        format!("{} {}", name, "varchar(525)")
    }
}

impl DBfield for u32 {
    fn create_db_fields(&self, name: String) -> String {
        format!("{} {}", name, "integer")
    }
}

impl DBfield for bool {
    fn create_db_fields(&self, name: String) -> String {
        format!("{} {}", name, "boolean")
    }
}
