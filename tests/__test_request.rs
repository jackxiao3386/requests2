use requests2::{Requests, Cache, Store, Value};
use rayon::prelude::*;


#[test]
pub fn run_requests_urls_with_parsers() {
    let data = Cache::new();
    let client = Requests::new(&data);
    let urls = [
        "https://www.baidu.com",
        "https://www.qq.com",
        "https://www.163.com",
    ];
    let _ = urls
        .par_iter()
        .map(|url| {
            let p = client.connect(url).default_headers().send().unwrap();
            p.parser(
                |p| {
                    p.find_all(
                        "a",
                        |f| f.attr("href").map_or(false, |v| v.starts_with("http://")),
                        "href",
                    )
                },
                format!("{}_link", url).as_str(),
            );

            p.parser(
                |p| p.find("title", |f| f.text() != "", "text"),
                format!("{}_title", url).as_str(),
            );
        })
        .map(|_| String::from(""))
        .collect::<String>();

    match data.get("https://www.qq.com_title") {
        Value::STR(i) => assert_eq!(i, "腾讯网"),
        _ => panic!(""),
    };

    if let Value::STR(i) = data.get("https://www.163.com_title") {
        assert_eq!(i, "网易");
    }

    if let Value::STR(i) = data.get("https://www.baidu.com_title") {
        assert_eq!(i, "百度一下，你就知道");
    }
}


#[test]
pub fn run_header_with_json() {
    let headers = r#"{"user-agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36"}"#;
    let store = Cache::new();
    let client = Requests::new(&store);
    let p = client.connect("https://www.qq.com").headers(headers).send().unwrap();
    p.parser(
        |p| p.find("title", |f| !f.text().is_empty(), "text"),
        "title",
    );
    store.print_json();
}


#[test]
    fn test_find_all() {
        let data = Cache::new();
        let client = Requests::new(&data);
        let rq = client.connect("https://www.qq.com/").default_headers().send().unwrap();

        rq.parser(|p| {
            p.find_all("a", |x| {
                x.attr("href").map_or(false, |v| v.starts_with("http"))
            }, "href")
        }, "href");

        rq.parser(|p| {
            p.find_all("li", |x| {
                x.attr("class").map_or(false, |v| v.contains("nav-item"))
            }, "text")
        }, "text");

        data.print();
       
    }
