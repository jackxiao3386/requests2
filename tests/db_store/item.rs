use dbfile_derive::{DBfile, dbnote};

#[derive(DBfile)]
#[dbnote(table_name = "test7", driver = "sqlite", primary_key = "href")]
pub struct Item<'a> {
    pub book_name: &'a str,
    pub href: String,
    pub img: String,
    pub isbn: String,
    pub price: f32,
}