use dbfile::DBfile;
use dbfile_derive::{DBfile, dbnote};

pub mod item;
pub mod si;

use si::SI;
use item::Item;


#[derive(DBfile)]
#[dbnote(table_name = "test1.csv")]
struct Person<'a>{
    name: &'a str,
    age: u32,
}


pub fn run_write_csv() {
    let p1 = Person {
       name: "zhangsan",
       age: 25,
    };

    let p2 = Person {
        name: "lisi",
        age: 32
    };

    let p3 = Person {
        name: "wangwu",
        age: 33
    };
    
    p1.write_csv_head();
    p1.to_csv("a");
    p2.to_csv("a");
    p3.to_csv("a");
   
}

pub fn run_test_postgres() {
    let by_data = SI {
        isbn: "113343".to_string(),
        price: 6.5,
    };
    by_data.create_table();
    by_data.to_db();
}

pub fn run_test_sqlite() {
    let by_data = Item {
        book_name: "test",
        href: "https://www.example23.com".to_string(),
        img: "https://www.fiwwgga.jpg".to_string(),
        isbn: "113343".to_string(),
        price: 6.5,
    };
    by_data.create_table();
    by_data.to_db();
}