use dbfile_derive::{DBfile, dbnote};

#[derive(DBfile)]
#[dbnote(table_name = "b337", driver = "postgres", primary_key = "isbn")]
pub struct SI {
    pub isbn: String,
    pub price: f32,
}