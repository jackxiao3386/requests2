use requests2::{Cache, Requests, Store};

#[test]
fn test_request_my_request() {
    let data = Cache::new();
    let client = Requests::new(&data);
    client
        .connect("https://www.youtube.com")
        .proxies("http://127.0.0.1:8889")
        .default_headers()
        .send()
        .unwrap();
}

#[test]
fn test_find_target_href() {
    let data = Cache::new();
    let client = Requests::new(&data);
    let rq = client
        .connect("https://www.qq.com")
        .default_headers()
        .proxies("http://127.0.0.1:8889")
        .send()
        .unwrap();

    rq.parser(
        |p| {
            p.find_all(
                "a",
                |n| {
                    n.attr("href").map_or(false, |x| x.starts_with("http:"))
                        && n.parent().map_or(false, |x| {
                            x.attr("class").map_or(false, |x| x.contains("nav-item"))
                        })
                },
                "href",
            )
        },
        "target_a_href",
    );

    data.print();
    let count = data.count("target_a_href");
    assert_eq!(count, 6);
}
