
mod db_store;
mod css_selector;
mod requests_and_parser;


#[test]
fn test_db_store() {
    // test this usage: cargo test --test tests -- test_db_store 
    db_store::run_write_csv();
    db_store::run_test_postgres();
    db_store::run_test_sqlite();
}

#[test]
fn test_css_selector() {
    css_selector::run_selector();
}   

#[test]
fn test_requests() {
    requests_and_parser::run_store_get();
    requests_and_parser::run_requests_urls();
    requests_and_parser::run_header_with_json();
    requests_and_parser::run_requests_urls_with_parsers();
}


#[test]
fn test_free_parse_and_store_to_csv() {
    css_selector::run_free_parse_and_store_to_csv();
}